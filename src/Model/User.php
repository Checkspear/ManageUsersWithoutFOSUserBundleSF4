<?php
/**
 * Created by PhpStorm.
 * User: checkspear
 * Date: 28/05/18
 * Time: 11:46
 */

namespace App\Model;

use Symfony\Component\Validator\Constraints as Assert;

class User
{
    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner votre nom.")
     */
    public $nom;

    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner votre prénom.")
     */
    public $prenom;

    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner votre pseudonyme qui sera visible par tous.")
     */
    public $username;

    /**
     * @var string
     * @Assert\Email(message="Veuillez renseigner un email valide.")
     */
    public $email;

    /**
     * @var string
     * @Assert\NotNull(message="Veuillez renseigner un mot de passe.")
     */
    public $password;
}