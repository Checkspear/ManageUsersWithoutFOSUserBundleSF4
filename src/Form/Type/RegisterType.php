<?php
/**
 * Created by PhpStorm.
 * User: checkspear
 * Date: 28/05/18
 * Time: 11:40
 */

namespace App\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, ['attr' =>['placeholder' => 'Nom']])
            ->add('prenom', TextType::class, ['attr' =>['placeholder' => 'Prénom']])
            ->add('username', TextType::class, ['attr' =>['placeholder' => 'Pseudonyme']])
            ->add('email', EmailType::class, ['attr' =>['placeholder' => 'Email']])
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Répéter le  Password'),
            ))
            ->add('submit', SubmitType::class, [
                    'label' => 'S\'enregistrer',
                    'attr' => ['class' => 'btn btn-primary btn-lg']]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Model\User'
        ));
    }
}