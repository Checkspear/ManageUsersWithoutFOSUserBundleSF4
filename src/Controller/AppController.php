<?php
/**
 * Created by PhpStorm.
 * User: checkspear
 * Date: 06/06/18
 * Time: 15:30
 */

namespace App\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AppController extends Controller
{
    /**
     * @Route("/list", name="list")
     */
    public function list(): Response
    {
        return $this->render('list.html.twig');
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('index.html.twig');
    }
}